# The Optimal Wedding (with Pyomo)

This repository contains a jupyter notebook presenting a mathematical optimization model for a seating 
arrangement problem written in Python and [Pyomo](http://www.pyomo.org/): `OptimalWedding.ipynb`

## Getting started

You first need to clone the repository onto your local system, for example using https:

```bash
>>> git clone https://gitlab.com/Rikerl/optimalwedding.git path/to/the/directory/to/clone/to
```

To run the notebook, you need python (the code has been tested only with python 3.7) as well as the 
packages listed in the `requirements.txt` file (please install the versions indicated).

Moreover, you need a solver, i.e., a program capable of solving an optimization model and return the
result to python. Here, we use the open-source [CBC solver from the COIN-OR](https://www.coin-or.org/)
(COmputational INfrastructure for Operations Research). You can find installation instructions for 
example [here](https://calliope.readthedocs.io/en/stable/user/installation.html):

```bash
>>> conda install -c conda-forge coincbc
```

To start the notebook, you can run `jupyter notebook` or `jupyter lab`:

```bash
>>> jupyter lab 
```

and click on the url to open the notebook in an internet browser.

